# Functional Curation -- Web Interface

This is the code for running Functional Curation websites.
Our prototype installation is publicly visible at [https://chaste.cs.ox.ac.uk/FunctionalCuration](https://chaste.cs.ox.ac.uk/FunctionalCuration).

## Installation

See [INSTALL.md](src/master/INSTALL.md).

## Documentation for developers

A few pointers are available at [Developing.md](src/master/Developing.md).
